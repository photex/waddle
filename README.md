# Waddle

This is a plugin being written for Houdini 15.5 which allows you to load Doom1/Doom2 wad files. It's still being written and is only being created for learning and curiosity.

## Current status

![map24](map24.png "What you can expect to see currently.")
