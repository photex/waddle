#include "SOP_WAD.hpp"

#include <UT/UT_DSOVersion.h> // unused, but required

#include <OP/OP_Operator.h>
#include <OP/OP_OperatorTable.h>
#include <PRM/PRM_Include.h>
#include <GU/GU_Detail.h>
#include <GEO/GEO_PrimPoly.h>
#include <UT/UT_Interrupt.h>


#define PN_FILENAME "file"
static PRM_Name filename_param(PN_FILENAME, "WAD File");
static PRM_Default filename_param_default(0, "$HIP/doom2.wad");

#define PN_MAPNAME "map"
static PRM_Name mapname_param(PN_MAPNAME, "Map");
static PRM_Default mapname_param_default(0, "MAP01");

#define PN_SCALE "scale"
static PRM_Name scale_param(PN_SCALE, "Scale");
static PRM_Default scale_param_default(1.0);
static PRM_Range scale_param_range(PRM_RANGE_RESTRICTED, 0.005, PRM_RANGE_RESTRICTED, 1.0000);

#define PN_ENABLE_EXTENDED "extended_details"
static PRM_Name enable_extended_details_param(PN_ENABLE_EXTENDED, "Extended Attrs");
static PRM_Default enable_extended_details_param_default(1.0);

#define PN_ADD_LINEDEFS "create_linedefs"
static PRM_Name create_linedefs_param(PN_ADD_LINEDEFS, "Create Linedefs");
static PRM_Default create_linedefs_param_default(1.0);


PRM_Template SOP_WAD::op_param_templates[] = {
  PRM_Template(PRM_FILE, 1, &filename_param, &filename_param_default),
  PRM_Template(PRM_STRING, 1, &mapname_param, &mapname_param_default),
  PRM_Template(PRM_FLT, 1, &scale_param, &scale_param_default, nullptr, &scale_param_range),
  PRM_Template(PRM_TOGGLE, 1, &enable_extended_details_param, &enable_extended_details_param_default),
  PRM_Template(PRM_TOGGLE, 1, &create_linedefs_param, &create_linedefs_param_default),
  PRM_Template()
};

OP_Node* SOP_WAD::create(OP_Network* net, const char* name, OP_Operator *op) {
  return new SOP_WAD(net, name, op);
}

SOP_WAD::SOP_WAD(OP_Network* net, const char* name, OP_Operator *op)
  : SOP_Node(net, name, op),
	scale(1.0),
	add_extended_detail_attributes(true),
  create_linedefs(true)
{
}

SOP_WAD::~SOP_WAD() {}

OP_ERROR SOP_WAD::cookMySop(OP_Context& context) {
  pointCache_t P_cache;
  sectorGrpCache_t sector_group_cache;

  auto now = context.getTime();

  if (error() >= UT_ERROR_ABORT) {
    return error();
  }

  evalString(wadpath, PN_FILENAME, 0, now);
  evalString(mapname, PN_MAPNAME, 0, now);
  scale = evalFloat(PN_SCALE, 0, now);
  add_extended_detail_attributes =
	  static_cast<bool>(evalInt(PN_ENABLE_EXTENDED, 0, now));
  create_linedefs =
    static_cast<bool>(evalInt(PN_ADD_LINEDEFS, 0, now));

  auto wad = Wad::load(wadpath.toStdString());
  if (!wad) {
    addError(SOP_MESSAGE, "Unable to load WAD.");
    return error();
  }

  wad::MapInfo map;
  try {
    map = wad->map_at(mapname.toStdString());
  } catch(const std::out_of_range&) {
    addError(SOP_MESSAGE, "Unable to find map.");
    return error();
  }

  gdp->clearAndDestroy();

  add_sector_groups(wad, map, &sector_group_cache);
  add_walls(wad, map, sector_group_cache);

  add_flats(wad, map, sector_group_cache);

  if (create_linedefs) {
    add_linedefs(wad, map, sector_group_cache, &P_cache);
  }

  if (add_extended_detail_attributes) {
    add_detail_attrs(wad, map);
  }

  return error();
}

GA_Offset SOP_WAD::add_line(const GA_Offset& from, const GA_Offset& to) const {
  auto *poly = GEO_PrimPoly::build(gdp, 2, true, false);
  poly->setPointOffset(0, from);
  poly->setPointOffset(1, to);
  return poly->getMapOffset();
}

GA_Offset SOP_WAD::add_point(const int16_t x, const int16_t y) const {
  auto ptoffset = gdp->appendPointOffset();
  fpreal vx = x;
  fpreal vz = -y;
  vx *= scale;
  vz *= scale;
  UT_Vector3R v(vx, 0.0, vz);
  gdp->setPos3(ptoffset, v);
  return ptoffset;
}

GA_Offset SOP_WAD::add_point(const int16_t x, const int16_t y, const int16_t z) const
{
  auto ptoffset = gdp->appendPointOffset();
  fpreal vx = x;
  fpreal vy = y;
  fpreal vz = -z;
  vx *= scale;
  vy *= scale;
  vz *= scale;
  UT_Vector3R v(vx, vy, vz);
  gdp->setPos3(ptoffset, v);
  return ptoffset;
}

GA_Offset SOP_WAD::add_point(const fpreal x, const fpreal y, const fpreal z) const {
  auto ptoffset = gdp->appendPointOffset();
  UT_Vector3R v(x, y, z);
  gdp->setPos3(ptoffset, v);
  return ptoffset;
}

GA_Offset SOP_WAD::add_quad(const wad::Vertex &from, const wad::Vertex &to, const int16_t y_min, const int16_t y_max) const
{
  fpreal vfx = from.x;
  vfx *= scale;
  fpreal vfz = -from.y;
  vfz *= scale;
  fpreal vtx = to.x;
  vtx *= scale;
  fpreal vtz = -to.y;
  vtz *= scale;

  fpreal vy_min = y_min;
  vy_min *= scale;
  fpreal vy_max = y_max;
  vy_max *= scale;

  auto np0 = add_point(vtx, vy_min, vtz);
  auto np1 = add_point(vfx, vy_min, vfz);
  auto np2 = add_point(vfx, vy_max, vfz);
  auto np3 = add_point(vtx, vy_max, vtz);

  auto *poly = GEO_PrimPoly::build(gdp, 4, false, false);
  poly->setPointOffset(0, np0);
  poly->setPointOffset(1, np1);
  poly->setPointOffset(2, np2);
  poly->setPointOffset(3, np3);
  return poly->getMapOffset();
}

void SOP_WAD::add_sector_groups(const Wad::Ptr_t& wad, const wad::MapInfo& map, sectorGrpCache_t* cache) const
{
  auto sector_count = wad->sector_count(map);
  for (auto sector_index=0; sector_index < sector_count; ++sector_index) {
    auto sector_name = name_for_index("sector_", sector_index);
    cache->emplace(std::make_pair(sector_index, gdp->newPrimitiveGroup(sector_name)));
  }
}

GA_Offset SOP_WAD::cached_vertex(const uint16_t index, const Wad::Ptr_t &wad, const wad::MapInfo& map,
                                 pointCache_t* cache) const
{
  GA_Offset ptoffset;
  auto cache_entry = cache->find(index);
  if (cache_entry == cache->end()) {
    const auto& vertex = wad->vertex_at(map, index);
    ptoffset = add_point(vertex.x, vertex.y);
    cache->emplace(std::make_pair(index, ptoffset));
  } else {
    ptoffset = cache_entry->second;
  }
  return ptoffset;
}

void SOP_WAD::add_detail_attrs(const Wad::Ptr_t &wad, const wad::MapInfo &map) const
{
  auto sector_count = wad->sector_count(map);
  for (auto i=0; i < sector_count; ++i) {
    auto sector_name = name_for_index("sector_", i);
    const auto& sector = wad->sector_at(map, i);
    add_sector_details(sector_name, sector);
  }

  auto sidedef_count = wad->sidedef_count(map);
  for (auto i=0; i < sidedef_count; ++i) {
    auto sidedef_name = name_for_index("sidedef_", i);
    const auto& sidedef = wad->sidedef_at(map, i);
    add_sidedef_details(sidedef_name, sidedef);
  }
}

void SOP_WAD::add_sector_details(const UT_String& sector_name, const wad::Sector &sector) const
{
  UT_String flats_attr_name(sector_name);
  flats_attr_name.append("_flats");
  UT_StringArray flats;
  flats.append(std::string(sector.floor_flat, 8).c_str());
  flats.append(std::string(sector.ceiling_flat, 8).c_str());
  GA_RWHandleSA flats_attr(gdp->addStringArray(GA_ATTRIB_DETAIL, flats_attr_name));
  flats_attr.set(GA_Offset(0), flats);

  UT_String heights_attr_name(sector_name);
  heights_attr_name.append("_heights");
  fpreal floor = sector.floor_height;
  fpreal ceiling = sector.ceiling_height;
  UT_FprealArray heights;
  heights.append(floor * scale);
  heights.append(ceiling * scale);
  GA_RWHandleDA heights_attr(gdp->addFloatArray(GA_ATTRIB_DETAIL, heights_attr_name, 1));
  heights_attr.set(GA_Offset(0), heights);
}

void SOP_WAD::add_sidedef_details(const UT_String &sidedef_name, const wad::SideDef &sidedef) const
{
  UT_String textures_attr_name(sidedef_name);
  textures_attr_name.append("_textures");
  UT_StringArray textures;
  textures.append(std::string(sidedef.upper, 8).c_str());
  textures.append(std::string(sidedef.middle, 8).c_str());
  textures.append(std::string(sidedef.lower, 8).c_str());
  GA_RWHandleSA textures_attr(gdp->addStringArray(GA_ATTRIB_DETAIL, textures_attr_name));
  textures_attr.set(GA_Offset(0), textures);

  UT_String sector_attr_name(sidedef_name);
  sector_attr_name.append("_sector");
  GA_RWHandleI sector_attr(gdp->addIntTuple(GA_ATTRIB_DETAIL, sector_attr_name, 1));
  sector_attr.set(GA_Offset(0), sidedef.sector);
}

void SOP_WAD::add_linedefs(const Wad::Ptr_t &wad, const wad::MapInfo &map,
                           const sectorGrpCache_t& sector_groups, pointCache_t* P_cache) const
{
  GA_RWHandleI front_attr(gdp->addIntTuple(GA_ATTRIB_PRIMITIVE, "front", 1, GA_Defaults(-1)));
  GA_RWHandleI back_attr(gdp->addIntTuple(GA_ATTRIB_PRIMITIVE, "back", 1, GA_Defaults(-1)));
  GA_RWHandleR floor_attr(gdp->addFloatTuple(GA_ATTRIB_PRIMITIVE, "floor", 1, GA_Defaults(0)));
  GA_RWHandleR ceiling_attr(gdp->addFloatTuple(GA_ATTRIB_PRIMITIVE, "ceiling", 1, GA_Defaults(0)));

  auto linedefs_grp = gdp->newPrimitiveGroup("linedefs");

  auto line_count = wad->line_count(map);
  for (auto i=0; i < line_count; ++i) {
    const auto& linedef = wad->linedef_at(map, i);

    auto from = cached_vertex(linedef.from, wad, map, P_cache);
    auto to = cached_vertex(linedef.to, wad, map, P_cache);

    if (linedef.front != 0xFFFF) {
      auto prim_offset = add_line(from, to);
      linedefs_grp->addOffset(prim_offset);
      const auto& sidedef = wad->sidedef_at(map, linedef.front);
      const auto& sector = wad->sector_at(map, sidedef.sector);

      auto* sector_group = sector_groups.at(sidedef.sector);
      sector_group->addOffset(prim_offset);

      front_attr.set(prim_offset, linedef.front);
      floor_attr.set(prim_offset, static_cast<fpreal>(sector.floor_height) * scale);
      ceiling_attr.set(prim_offset, static_cast<fpreal>(sector.ceiling_height) * scale);
    }

    if (linedef.back != 0xFFFF) {
      auto prim_offset = add_line(to, from);
      linedefs_grp->addOffset(prim_offset);
      const auto& sidedef = wad->sidedef_at(map, linedef.back);
      const auto& sector = wad->sector_at(map, sidedef.sector);

      auto* sector_group = sector_groups.at(sidedef.sector);
      sector_group->addOffset(prim_offset);

      back_attr.set(prim_offset, linedef.back);
      floor_attr.set(prim_offset, static_cast<fpreal>(sector.floor_height) * scale);
      ceiling_attr.set(prim_offset, static_cast<fpreal>(sector.ceiling_height) * scale);
    }
  }
}

void SOP_WAD::add_walls(const Wad::Ptr_t& wad, const wad::MapInfo& map, const sectorGrpCache_t& sector_groups) const {
  GA_RWHandleS texture_attr(gdp->addStringTuple(GA_ATTRIB_PRIMITIVE, "texture", 1));
  auto line_count = wad->line_count(map);
  for (auto linedef_index=0; linedef_index < line_count; ++linedef_index) {
    const auto& linedef = wad->linedef_at(map, linedef_index);

    const auto& from = wad->vertex_at(map, linedef.from);
    const auto& to = wad->vertex_at(map, linedef.to);

    bool has_front = linedef.front != 0xFFFF;
    bool has_back = linedef.back != 0xFFFF;

    if (has_front) {
      const auto& front = wad->sidedef_at(map, linedef.front);
      const auto& front_sector = wad->sector_at(map, front.sector);
      auto front_floor = front_sector.floor_height;
      auto front_ceiling = front_sector.ceiling_height;
      auto* front_sector_group = sector_groups.at(front.sector);
      if (has_back) {
        const auto& back = wad->sidedef_at(map, linedef.back);
        const auto& back_sector = wad->sector_at(map, back.sector);
        auto* back_sector_group = sector_groups.at(back.sector);
        auto back_floor = back_sector.floor_height;
        auto back_ceiling = back_sector.ceiling_height;
        if (front.upper[0] != '-') {
          auto prim_offset = add_quad(from, to, back_ceiling, front_ceiling);
          front_sector_group->addOffset(prim_offset);
          texture_attr.set(prim_offset, std::string(front.upper, 8).c_str());
        } else {
          auto prim_offset = add_quad(to, from, front_ceiling, back_ceiling);
          back_sector_group->addOffset(prim_offset);
          texture_attr.set(prim_offset, std::string(back.upper, 8).c_str());
        }
        if (front.middle[0] != '-') {
          auto prim_offset = add_quad(from, to, back_floor, back_ceiling);
          front_sector_group->addOffset(prim_offset);
          texture_attr.set(prim_offset, std::string(front.middle, 8).c_str());
        }
        if (front.lower[0] != '-') {
          auto prim_offset = add_quad(from, to, front_floor, back_floor);
          front_sector_group->addOffset(prim_offset);
          texture_attr.set(prim_offset, std::string(front.lower, 8).c_str());
        } else {
          auto prim_offset = add_quad(to, from, back_floor, front_floor);
          back_sector_group->addOffset(prim_offset);
          texture_attr.set(prim_offset, std::string(back.upper, 8).c_str());
        }
      } else {
        //if (front.upper[0] != '-') {}
        if (front.middle[0] != '-') {
          auto prim_offset = add_quad(from, to, front_floor, front_ceiling);
          front_sector_group->addOffset(prim_offset);
          texture_attr.set(prim_offset, std::string(front.middle, 8).c_str());
        }
        //if (front.lower[0] != '-') {}
      }
    }
  }
}

void SOP_WAD::add_flats(const Wad::Ptr_t& wad, const wad::MapInfo& map, const sectorGrpCache_t& sector_groups) const
{
  //std::unordered_map<uint32_t, std::set<uint32_t>> lines_for_sector;
  //
  //// DRY the storage of line indexes
  //auto update_cache = [&lines_for_sector, &wad, &map](const uint32_t sidedef_index, const uint32_t line_index)
  //{
  //  const auto& sidedef = wad->sidedef_at(map, sidedef_index);
  //  auto cache = lines_for_sector.find(sidedef.sector);
  //  cache->second.insert(line_index);
  //};
  //
  //// For each line, get any sidedefs and update cache
  //auto line_count = wad->line_count(map);
  //for (auto line_index=0; line_index < line_count; ++line_index) {
  //  const auto& linedef = wad->linedef_at(map, line_index);
  //  if (linedef.front != 0xFFFF) {
  //    update_cache(linedef.front, line_index);
  //  }
  //}
  //
  //auto sector_count = wad->sector_count(map);
  //for (auto sector_index=0; sector_index < sector_count; ++sector_index) {
  //  const auto& sector = wad->sector_at(map, sector_index);
  //}
}

UT_String SOP_WAD::name_for_index(const std::string &prefix, const uint32_t &index) const
{
  UT_String name(prefix);
  auto index_str = std::to_string(index);
  name.append(index_str.c_str(), static_cast<int>(index_str.length()));
  return name;
}
