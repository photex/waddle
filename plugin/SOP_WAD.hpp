#pragma once

#include <unordered_map>
#include <memory>
#include <SOP/SOP_Node.h>

#include <wadlib.hpp>

class OP_TemplatePair;

class OP_VariablePair;

class SOP_WAD : public SOP_Node
{
  using pointCache_t = std::unordered_map<uint16_t, GA_Offset>;
  using sectorGrpCache_t = std::unordered_map<uint32_t, GA_PrimitiveGroup*>;
public:

  static PRM_Template		 op_param_templates[];

  static OP_Node *create(OP_Network *net, const char *name, OP_Operator *op);

protected:
  SOP_WAD(OP_Network *net, const char *name, OP_Operator *entry);

  virtual ~SOP_WAD();

  OP_ERROR cookMySop(OP_Context &context) override;

private:
  UT_String wadpath;
  UT_String mapname;
  fpreal scale;
  bool add_extended_detail_attributes;
  bool create_linedefs;

  GA_Offset add_line(const GA_Offset& from, const GA_Offset& to) const;
  GA_Offset add_point(const int16_t x, const int16_t y) const;
  GA_Offset add_point(const int16_t x, const int16_t y, const int16_t z) const;
  GA_Offset add_point(const fpreal x, const fpreal y, const fpreal z) const;
  GA_Offset add_quad(const wad::Vertex& from, const wad::Vertex& to,
                     const int16_t y_min, const int16_t y_max) const;

  GA_Offset cached_vertex(const uint16_t index, const Wad::Ptr_t& wad, const wad::MapInfo& map, pointCache_t* cache) const;

  void add_detail_attrs(const Wad::Ptr_t& wad, const wad::MapInfo& map) const;
  void add_sector_details(const UT_String& sector_name, const wad::Sector& sector) const;
  void add_sidedef_details(const UT_String& sidedef_name, const wad::SideDef& sidedef) const;
  void add_sector_groups(const Wad::Ptr_t& wad, const wad::MapInfo& map, sectorGrpCache_t* cache) const;
  void add_walls(const Wad::Ptr_t& wad, const wad::MapInfo& map, const sectorGrpCache_t& sector_groups) const;
  void add_flats(const Wad::Ptr_t& wad, const wad::MapInfo& map, const sectorGrpCache_t& sector_groups) const;

  UT_String name_for_index(const std::string& prefix, const uint32_t& index) const;

  void add_linedefs(const Wad::Ptr_t& wad, const wad::MapInfo& map,
                    const sectorGrpCache_t& sector_groups, pointCache_t* P_cache) const;
};
