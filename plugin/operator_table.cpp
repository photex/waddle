#include "SOP_WAD.hpp"

#include <OP/OP_OperatorTable.h>

// Houdini calls this when the plugin is loaded in order to add the operator(s)
// to the op table.
void newSopOperator(OP_OperatorTable *table)
{
    table->addOperator(
        new OP_Operator(
            "read_wad",
            "WAD",
            SOP_WAD::create,
            SOP_WAD::op_param_templates,
            0,
            0,
            0,
            OP_FLAG_GENERATOR
        )
    );
}
