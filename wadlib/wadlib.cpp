#include "wadlib.hpp"

#include <algorithm>
#include <cstring>

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#endif

#include <assert.h>


Wad::Ptr_t Wad::load(const std::string& path) {
#ifdef _WIN32
  DWORD hint = FILE_FLAG_RANDOM_ACCESS;
  HANDLE file_handle = CreateFileA(path.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, hint, NULL);
  if (file_handle) {
    LARGE_INTEGER filesize_result;
    if (GetFileSizeEx(file_handle, &filesize_result)) {
      auto filesize = static_cast<uint64_t>(filesize_result.QuadPart);
      auto file_mapping = CreateFileMapping(file_handle, NULL, PAGE_READONLY, 0, 0, NULL);
      void* data = MapViewOfFile(file_mapping, FILE_MAP_READ, 0, 0, filesize);
      if (data) {
        return Ptr_t(
          new Wad(
            mmapDataPtr_t(
              static_cast<char*>(data),
              [filesize, file_handle, file_mapping](char* data) {
                UnmapViewOfFile(data);
                CloseHandle(file_handle);
              })
            ));
      }
    }
  }
#else
  struct stat st; stat(path.c_str(), &st);
  size_t filesize = st.st_size;
  int fd = open(path.c_str(), O_RDONLY, 0);
  if (fd != -1) {
    void *data = mmap(nullptr, filesize, PROT_READ, MAP_PRIVATE, fd, 0);
    if (data != MAP_FAILED) {
      return Ptr_t(
        new Wad(
          mmapDataPtr_t(
            static_cast<char*>(data),
            [filesize, fd](char* data) {
              int rc = munmap(data, filesize);
              close(fd);
            })
          ));
    }
  }
#endif
  return Ptr_t(nullptr);
}

Wad::Wad(mmapDataPtr_t&& buffer_):
  buffer(std::move(buffer_))
{
  _lump_count = *reinterpret_cast<int32_t*>(buffer.get() + 0x04);
  _directory_info_offset = *reinterpret_cast<int32_t*>(buffer.get() + 0x08);

  for (uint32_t i=0; i < _lump_count; ++i) {
    auto offset = _directory_info_offset + (i * LUMP_INFO_SIZE);
    char* lump_info_start = buffer.get() + offset;

    const wad::LumpInfo* entry = reinterpret_cast<const wad::LumpInfo*>(lump_info_start);

    _lumps.push_back(entry);

    if (std::memcmp("MAP", entry->name, 3) == 0 ||
        (entry->name[0] == 'E' && entry->name[2] == 'M')) {
      auto index = (uint32_t)_lumps.size()-1;
      cache_map(entry->name, index);
    }
  }
}

void Wad::cache_map(const std::string& name, uint32_t index) {
  wad::MapInfo map;
  map.index = index;
  map.things = map.index+1;
  map.linedefs = map.index+2;
  map.sidedefs = map.index+3;
  map.vertexes = map.index+4;
  map.segs = map.index+5;
  map.ssectors = map.index+6;
  map.nodes = map.index+7;
  map.sectors = map.index+8;
  map.reject = map.index+9;
  map.blockmap = map.index+10;
  _maps.emplace(std::make_pair(name, map));
}

uint32_t Wad::map_count() const {
  return (uint32_t)_maps.size();
}

uint32_t Wad::lump_count() const {
  return (uint32_t)_lump_count;
}

const wad::LumpInfo& Wad::lump_at(const uint32_t& index) const {
  return *_lumps.at(index);
}

std::vector<std::string> Wad::map_names() const {
  std::vector<std::string> names;
  for (const auto& item : _maps) {
    names.push_back(item.first);
  }
  return std::move(names);
}

const wad::MapInfo& Wad::map_at(const std::string& name) const {
  return _maps.at(name);
}

uint32_t Wad::line_count(const wad::MapInfo& map) const {
  const auto& linedefs = lump_at(map.linedefs);
  return linedefs.size / sizeof(wad::LineDef);
}

const wad::LineDef& Wad::linedef_at(const wad::MapInfo& map, uint32_t index) const {
  const auto& linedefs = lump_at(map.linedefs);
  return *reinterpret_cast<wad::LineDef*>(buffer.get() + (linedefs.offset + (index * sizeof(wad::LineDef))));
}

uint32_t Wad::vertex_count(const wad::MapInfo& map) const {
  const auto& vertexes = lump_at(map.vertexes);
  return vertexes.size / sizeof(wad::Vertex);
}

const wad::Vertex& Wad::vertex_at(const wad::MapInfo& map, uint32_t index) const {
  const auto& vertexes = lump_at(map.vertexes);
  return *reinterpret_cast<wad::Vertex*>(buffer.get() + (vertexes.offset + (index * sizeof(wad::Vertex))));
}

uint32_t Wad::sector_count(const wad::MapInfo& map) const {
  const auto& sectors = lump_at(map.sectors);
  return sectors.size / sizeof(wad::Sector);
}

const wad::Sector& Wad::sector_at(const wad::MapInfo& map, uint32_t index) const {
  const auto& sectors = lump_at(map.sectors);
  return *reinterpret_cast<wad::Sector*>(buffer.get() + (sectors.offset + (index * sizeof(wad::Sector))));
}

uint32_t Wad::subsector_count(const wad::MapInfo& map) const {
  const auto& subsectors = lump_at(map.ssectors);
  return subsectors.size / sizeof(wad::SubSector);
}

const wad::SubSector& Wad::subsector_at(const wad::MapInfo& map, uint32_t index) const {
  const auto& subsectors = lump_at(map.ssectors);
  return *reinterpret_cast<wad::SubSector*>(buffer.get() + (subsectors.offset + (index * sizeof(wad::SubSector))));
}

uint32_t Wad::segment_count(const wad::MapInfo& map) const {
  const auto& segments = lump_at(map.segs);
  return segments.size / sizeof(wad::Segment);
}

const wad::Segment& Wad::segment_at(const wad::MapInfo& map, const wad::SubSector& subsector, uint32_t index) const {
  const auto& segments = lump_at(map.segs);
  auto segment_size = sizeof(wad::Segment);
  auto segstart = buffer.get() + (segments.offset + ((subsector.first_seg + index) * segment_size));
  return *reinterpret_cast<wad::Segment*>(segstart);
}

uint32_t Wad::sidedef_count(const wad::MapInfo& map) const {
  const auto& sidedefs = lump_at(map.sidedefs);
  return sidedefs.size / sizeof(wad::SideDef);
}

const wad::SideDef& Wad::sidedef_at(const wad::MapInfo& map, uint32_t index) const {
  const auto& sidedefs = lump_at(map.sidedefs);
  return *reinterpret_cast<wad::SideDef*>(buffer.get() + (sidedefs.offset + (index * sizeof(wad::SideDef))));
}
