#pragma once

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include <functional>

#define LUMP_INFO_SIZE 16
#define LUMP_NAME_MAX 8

namespace wad {

struct MapInfo {
  int32_t index;
  int32_t things;
  int32_t linedefs;
  int32_t sidedefs;
  int32_t vertexes;
  int32_t segs;
  int32_t ssectors;
  int32_t nodes;
  int32_t sectors;
  int32_t reject;
  int32_t blockmap;
};
using mapVisitor_t = std::function<void (const MapInfo& info)>;

struct LumpInfo {
  uint32_t offset;
  uint32_t size;
  char name[8];
};

const uint16_t LDF_IMPASSIBLE     = 0x00;
const uint16_t LDF_BLOCK_MONSTERS = 0x01;
const uint16_t LDF_TWO_SIDED      = 0x02;
const uint16_t LDF_UPPER_UNPEGGED = 0x04;
const uint16_t LDF_LOWER_UNPEGGED = 0x08;
const uint16_t LDF_SECRET         = 0x10;
const uint16_t LDF_BLOCK_SOUND    = 0x20;
const uint16_t LDF_NOT_ON_MAP     = 0x40;
const uint16_t LDF_ALREADY_ON_MAP = 0x80;

struct LineDef {
  uint16_t from;
  uint16_t to;
  uint16_t flags;
  uint16_t types;
  uint16_t tag;
  uint16_t front;
  uint16_t back;
};

struct SideDef {
  int16_t x;
  int16_t y;
  char upper[8];
  char lower[8];
  char middle[8];
  uint16_t sector;
};

struct Vertex {
  int16_t x;
  int16_t y;
};

struct Segment {
  uint16_t from;
  uint16_t to;
                 // BAMS: Binary Angle Measurement
  int16_t angle; // 0x0000=east, 0x4000=north, 0x8000=west, 0xc000=south
  uint16_t linedef;
  int16_t direction;
  int16_t offset;
};

struct SubSector {
  int16_t seg_count;
  int16_t first_seg;
};

struct Node {
  int16_t x;
  int16_t y;
  int16_t dx;
  int16_t dy;
  // All SEGS in right child of node must be within this box
  int16_t right_y_max;
  int16_t right_y_min;
  int16_t right_x_min;
  int16_t right_x_max;
  // All SEGS in left child of node must be within this box
  int16_t left_y_max;
  int16_t left_y_min;
  int16_t left_x_min;
  int16_t left_x_max;
  // Children
  int16_t right_child;
  int16_t left_child;
};

struct Sector {
  int16_t floor_height;
  int16_t ceiling_height;
  char floor_flat[8];
  char ceiling_flat[8];
  int16_t lightlevel;
  uint16_t special_sector;
  uint16_t tag;
};

struct BlockMap {
  int16_t x;
  int16_t y;
  int16_t columns;
  int16_t rows;
};

}

class Wad {
  using mmapCleanUpFn_t = std::function<void(char*)>;
  using mmapDataPtr_t = std::unique_ptr<char, mmapCleanUpFn_t>;
public:
  using Ptr_t = std::unique_ptr<Wad>;

  static Ptr_t load(const std::string& path);

  uint32_t map_count() const;
  std::vector<std::string> map_names() const;
  void foreach_map(const wad::mapVisitor_t& visitor) const;
  const wad::MapInfo& map_at(const std::string& name) const;

  uint32_t lump_count() const;
  const wad::LumpInfo& lump_at(const uint32_t& index) const;

  uint32_t line_count(const wad::MapInfo& map) const;
  const wad::LineDef& linedef_at(const wad::MapInfo& map, uint32_t index) const;

  uint32_t vertex_count(const wad::MapInfo& map) const;
  const wad::Vertex& vertex_at(const wad::MapInfo& map, uint32_t index) const;

  uint32_t sector_count(const wad::MapInfo& map) const;
  const wad::Sector& sector_at(const wad::MapInfo& map, uint32_t index) const;

  uint32_t subsector_count(const wad::MapInfo& map) const;
  const wad::SubSector& subsector_at(const wad::MapInfo& map, uint32_t index) const;

  uint32_t segment_count(const wad::MapInfo& map) const;
  const wad::Segment& segment_at(const wad::MapInfo& map, const wad::SubSector& subsector, uint32_t index) const;

  uint32_t sidedef_count(const wad::MapInfo& map) const;
  const wad::SideDef& sidedef_at(const wad::MapInfo& map, uint32_t index) const;

private:
  Wad(mmapDataPtr_t&& data);
  Wad(const Wad& other) = delete;

  uint32_t _lump_count;
  uint32_t _directory_info_offset;

  std::unordered_map<std::string, wad::MapInfo> _maps;
  std::vector<const wad::LumpInfo*> _lumps;

  void cache_map(const std::string& name, uint32_t index);

  mmapDataPtr_t buffer;
};
