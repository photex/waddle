
#include <iostream>
#include <cstring>
#include <algorithm>
#include "wadlib.hpp"


int main(int argc, char* argv[]) {
  if (argc != 2) {
    std::cout << "please specify the IWAD to load" << std::endl;
    return 1;
  }

  std::string wad_path(argv[1]);

  auto wad = Wad::load(wad_path);
  if (!wad) {
    std::cout << "Unable to load WAD." << std::endl;
    return 2;
  }

  auto map_count = wad->map_count();
  auto lump_count = wad->lump_count();

  std::cout << "WAD: " << wad_path << ", Contains "
            << map_count << " maps and "
            << lump_count << " lumps."
            << std::endl;

  // wad->foreach_lump([](const wad::LumpInfo& lump) {
  //     int len = std::strlen(lump.name);
  //     std::string name(lump.name, std::min(LUMP_NAME_MAX, len));
  //     std::cout << name << std::endl;
  //   });

  // std::vector<std::string> map_names = wad->map_names();
  // for (auto& name : map_names) {
  //   std::cout << name << std::endl;
  // }

  const auto& map01 = wad->map_at("MAP01");
  auto line_count = wad->line_count(map01);
  auto vertex_count = wad->vertex_count(map01);
  auto sector_count = wad->sector_count(map01);
  std::cout << "MAP01: has " << line_count << " lines, " << vertex_count << " vertices, " << sector_count << " sectors." << std::endl;
  // for (auto i=0; i < vertex_count; ++i) {
  //   const auto& wad_vertex = wad->vertex_at(map01, i);
  //   std::cout << "vert: " << i <<  " x: " << wad_vertex.x << ", y: " << wad_vertex.y << std::endl;
  // }

  const auto& sector0 = wad->sector_at(map01, 9);
  std::cout << "Sector0 floor: " << sector0.floor_height << ", ceiling: " << sector0.ceiling_height << std::endl;

  const auto& ssector = wad->subsector_at(map01, 0);
  auto segment_count = ssector.seg_count;
  for (auto segment_index=0; segment_index < segment_count; ++segment_index) {
    const auto& segment = wad->segment_at(map01, ssector, segment_index);
    std::cout << "Segment " << segment_index << " - "
              << "from: " << segment.from << ", to: " << segment.to
              << ", linedef: " << segment.linedef
              << ", angle: " << segment.angle << ", direction: " << segment.direction
              << ", offset: " << segment.offset
              << std::endl;
  }

  return 0;
}
